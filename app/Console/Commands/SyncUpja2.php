<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SyncUpja2 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:upja2';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Synchronization Upja2 Database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        goto test;
        test:
        /* Sync Province */
        echo "Fetch Province\n";
        app('db')->connection('mysql')->select("SET foreign_key_checks = 0");
        app('db')->connection('mysql')->table('ms_province')->delete();
        $items = app('db')->connection('mysql2')->table('ms_province')->select('*')->get();
        $inserts = [];
        foreach($items as $item) {
            $inserts[] = [
                'id_province' => $item->id_province,
                'province_name' => $item->province_name,
                'province_hide' => $item->province_hide,
                'province_created' => $item->province_created,
                'province_updated' => $item->province_updated,
            ];
        }
        try {
            app('db')->beginTransaction();
            $insert = app('db')->connection('mysql')->table('ms_province')->insert($inserts);
            if($insert) {
                echo "save ".count($inserts)." data\n";
            } else {
                echo "failed\n";
            }
            app('db')->commit();
        } catch(Exception $e) {
            app('db')->rollback();
            echo $e->getMessage();
        }
        /* Sync District */
        echo "Fetch District\n";
        app('db')->connection('mysql')->select("SET foreign_key_checks = 0");
        app('db')->connection('mysql')->table('ms_district')->delete();
        $items = app('db')->connection('mysql2')->table('ms_district')->select('*')->get();
        $inserts = [];
        foreach($items as $item) {
            $inserts[] = [
                'id_district' => $item->id_district,
                'id_province' => $item->id_province,
                'district_name' => $item->district_name,
                'district_hide' => $item->district_hide,
                'district_created' => $item->district_created,
                'district_updated' => $item->district_updated,
            ];
        }
        try {
            app('db')->beginTransaction();
            $insert = app('db')->connection('mysql')->table('ms_district')->insert($inserts);
            if($insert) {
                echo "save ".count($inserts)." data\n";
            } else {
                echo "failed\n";
            }
            app('db')->commit();
        } catch(Exception $e) {
            app('db')->rollback();
            echo $e->getMessage();
        }
        /* Sync Sub District */
        echo "Fetch Sub District\n";
        app('db')->connection('mysql')->select("SET foreign_key_checks = 0");
        app('db')->connection('mysql')->table('ms_subdistrict')->delete();
        $items = app('db')->connection('mysql2')->table('ms_subdistrict')->select('*')->get();
        $inserts = [];
        foreach($items as $item) {
            $inserts[] = [
                'id_subdistrict' => $item->id_subdistrict,
                'id_district' => $item->id_district,
                'subdistrict_name' => $item->subdistrict_name,
                'subdistrict_hide' => $item->subdistrict_hide,
                'subdistrict_created' => $item->subdistrict_created,
                'subdistrict_updated' => $item->subdistrict_updated,
            ];
        }
        try {
            app('db')->beginTransaction();
            $insert = app('db')->connection('mysql')->table('ms_subdistrict')->insert($inserts);
            if($insert) {
                echo "save ".count($inserts)." data\n";
            } else {
                echo "failed\n";
            }
            app('db')->commit();
        } catch(Exception $e) {
            app('db')->rollback();
            echo $e->getMessage();
        }
        /* Sync Village */
        echo "Fetch Village\n";
        app('db')->connection('mysql')->select("SET foreign_key_checks = 0");
        app('db')->connection('mysql')->table('ms_village')->delete();
        $items = app('db')->connection('mysql2')->table('ms_village')->select('*')->get();
        $inserts = [];
        foreach($items as $item) {
            $inserts[] = [
                'id_village' => $item->id_village,
                'id_subdistrict' => $item->id_subdistrict,
                'village_name' => $item->village_name,
                'village_hide' => $item->village_hide,
                'village_created' => $item->village_created,
                'village_updated' => $item->village_updated,
            ];
        }
        try {
            app('db')->beginTransaction();
            $insert = app('db')->connection('mysql')->table('ms_village')->insert($inserts);
            if($insert) {
                echo "save ".count($inserts)." data\n";
            } else {
                echo "failed\n";
            }
            app('db')->commit();
        } catch(Exception $e) {
            app('db')->rollback();
            echo $e->getMessage();
        }
        /* Sync Upja */
        echo "Fetch Upja\n";
        app('db')->connection('mysql')->select("SET foreign_key_checks = 0");
        app('db')->connection('mysql')->table('ms_upja')->delete();

        app('db')->connection('mysql')->table('upja_uom')->delete();
        app('db')->connection('mysql')->table('upja_uom')->insert([
            [
                'id' => 1,
                'uom_name' => 'meter',
                'uom_desc' => 'meter',
                'uom_hide' => 0,
                'uom_created' => date('Y-m-d H:i:s'),
                'uom_updated' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 2,
                'uom_name' => 'menit',
                'uom_desc' => 'menit',
                'uom_hide' => 0,
                'uom_created' => date('Y-m-d H:i:s'),
                'uom_updated' => date('Y-m-d H:i:s'),
            ],
            [
                'id' => 3,
                'uom_name' => 'gram',
                'uom_desc' => 'gram',
                'uom_hide' => 0,
                'uom_created' => date('Y-m-d H:i:s'),
                'uom_updated' => date('Y-m-d H:i:s'),
            ],
        ]);

        $items = app('db')->connection('mysql2')->table('ms_user')->select('*')->where('user_level', 'upja')->get();
        $inserts = [];
        $increment = 1;
        foreach($items as $item) {
            $inserts[] = [
                'id_upja' => $item->id_user,
                'id_village' => $item->id_village,
                'upja_id' => 'UPJ'.substr(date('Y'), 2).str_pad($increment++, 4, 0, STR_PAD_LEFT),
                'upja_fullname' => $item->user_fullname,
                'upja_email' => $item->user_email,
                'upja_phone' => $item->user_phone,
                'upja_password' => $item->user_password,
                'upja_emergency_phone' => '',
                'upja_image' => $item->user_image,
                'upja_path' => '',
                'upja_address' => $item->user_address,
                'upja_status' => $item->user_active?'active':'inactive',
                'upja_verified' => 1,
                'upja_lat' => '',
                'upja_long' => '',
                'upja_hide' => $item->user_hide,
                'upja_created' => $item->user_created,
                'upja_updated' => $item->user_updated,
            ];
        }
        try {
            app('db')->beginTransaction();
            $insert = app('db')->connection('mysql')->table('ms_upja')->insert($inserts);
            if($insert) {
                echo "save ".count($inserts)." data\n";
            } else {
                echo "failed\n";
            }
            app('db')->commit();
        } catch(Exception $e) {
            app('db')->rollback();
            echo $e->getMessage();
        }
        /* Sync Farmer */
        echo "Fetch Farmer\n";
        app('db')->connection('mysql')->select("SET foreign_key_checks = 0");
        app('db')->connection('mysql')->table('ms_farmer')->delete();
        $items = app('db')->connection('mysql2')->table('ms_user')->select('*')->where('user_level', 'petani')->get();
        $inserts = [];
        $increment = 1;
        foreach($items as $item) {
            $inserts[] = [
                'id_farmer' => $item->id_user,
                'farmer_id' => 'FRM'.substr(date('Y'), 2).str_pad($increment++, 4, 0, STR_PAD_LEFT),
                'farmer_fullname' => $item->user_fullname,
                'farmer_email' => $item->user_email,
                'farmer_phone' => $item->user_phone,
                'farmer_password' => $item->user_password,
                'farmer_image' => $item->user_image,
                'farmer_status' => $item->user_active?'active':'inactive',
                'farmer_verified' => 1,
                'farmer_hide' => $item->user_hide,
                'farmer_created' => $item->user_created,
                'farmer_updated' => $item->user_updated,
            ];
        }
        try {
            app('db')->beginTransaction();
            $insert = app('db')->connection('mysql')->table('ms_farmer')->insert($inserts);
            if($insert) {
                echo "save ".count($inserts)." data\n";
            } else {
                echo "failed\n";
            }
            app('db')->commit();
        } catch(Exception $e) {
            app('db')->rollback();
            echo $e->getMessage();
        }
        /* Sync Olah Tanah */
        echo "Fetch Olah Tanah\n";
        app('db')->connection('mysql')->select("SET foreign_key_checks = 0");
        app('db')->connection('mysql')->table('ms_olah_tanah')->delete();
        $items = app('db')->connection('mysql2')->table('ms_olah_tanah')->select('*')->get();
        $inserts = [];
        $increment = 1;
        foreach($items as $item) {
            $inserts[] = [
                'id' => $item->id_ot,
                'id_upja' => $item->id_user,
                'created_by' => $item->id_user,
                'created_by_role' => 'upja',
                'title' => $item->ot_title,
                'seri' => $item->ot_seri,
                'image' => $item->ot_image,
                'price' => $item->ot_price,
                'order_min' => $item->ot_order_min,
                'stock' => $item->ot_stock,
                'terms' => $item->ot_term,
                'is_publish' => $item->ot_publish,
                'is_hide' => $item->ot_hide,
                'uom_list' => '',
                'uom_default' => 1,
                'created_at' => $item->ot_created,
                'updated_at' => $item->ot_updated,
            ];
        }
        try {
            app('db')->beginTransaction();
            $insert = app('db')->connection('mysql')->table('ms_olah_tanah')->insert($inserts);
            if($insert) {
                echo "save ".count($inserts)." data\n";
            } else {
                echo "failed\n";
            }
            app('db')->commit();
        } catch(Exception $e) {
            app('db')->rollback();
            echo $e->getMessage();
        }

        /* Sync Olah Tanah Implement */
        echo "Fetch Olah Tanah Impelement\n";
        app('db')->connection('mysql')->select("SET foreign_key_checks = 0");
        app('db')->connection('mysql')->table('ot_implement')->delete();
        $items = app('db')->connection('mysql2')->table('ot_implement')->select('*')->get();
        $inserts = [];
        $id_admin = app('db')->connection('mysql')->table('ms_admin')->select('*')->first();
        $id_admin = $id_admin?$id_admin->id_admin:1;
        foreach($items as $item) {
            $inserts[] = [
                'id' => $item->id_ot_implement,
                'id_ot' => $item->id_ot,
                'name' => $item->ot_implement_name,
                'price' => $item->ot_implement_price,
                'created_by' => $id_admin,
                'created_at' => $item->ot_implement_created,
                'updated_at' => $item->ot_implement_updated,
            ];
        }
        try {
            app('db')->beginTransaction();
            $insert = app('db')->connection('mysql')->table('ot_implement')->insert($inserts);
            if($insert) {
                echo "save ".count($inserts)." data\n";
            } else {
                echo "failed\n";
            }
            app('db')->commit();
        } catch(Exception $e) {
            app('db')->rollback();
            echo $e->getMessage();
        }
    }

    private function show_status($done, $total, $size=30) {

        static $start_time;

        // if we go over our bound, just ignore it
        if($done > $total) return;

        if(empty($start_time)) $start_time=time();
            $now = time();

        $perc=(double)($done/$total);

        $bar=floor($perc*$size);

        $status_bar="\r[";
        $status_bar.=str_repeat("=", $bar);
        if($bar<$size){
            $status_bar.=">";
            $status_bar.=str_repeat(" ", $size-$bar);
        } else {
            $status_bar.="=";
        }

        $disp=number_format($perc*100, 0);

        $status_bar.="] $disp%  $done/$total";

        $rate = ($now-$start_time)/$done;
        $left = $total - $done;
        $eta = round($rate * $left, 2);

        $elapsed = $now - $start_time;

        $status_bar.= " remaining: ".number_format($eta)." sec.  elapsed: ".number_format($elapsed)." sec.";

        echo "$status_bar  ";

        flush();

        // when done, send a newline
        if($done == $total) {
            echo "\n";
        }
    }
}
