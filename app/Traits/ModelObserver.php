<?php

namespace App\Traits;

use JWTAuth;
use Illuminate\Support\Facades\App;
use App\Services\Auth;

trait ModelObserver {

    public static function bootModelObserver()
    {
        static::creating(function ($model) {
            $auth = App::make(Auth::class);
            $user_id = $auth->user()->id;

            if(isset($model->date) && $model->date) {
                $model->date = date('Y-m-d', strtotime($model->date));
            }

            if($auth->role() != 'admin' && in_array('id_upja', $model->autoFill))
                $model->id_upja = $user_id;

            if($auth->role() != 'admin' && in_array('id_farmer', $model->autoFill))
                $model->id_farmer = $user_id;

            if(in_array('created_by_role', $model->autoFill))
                $model->created_by_role = $auth->role();

            if(in_array('created_by', $model->autoFill))
                $model->created_by = $user_id;

            if(in_array('updated_by', $model->autoFill))
                $model->updated_by = $user_id;

            if(in_array('branch_id', $model->autoFill) && $model->branch_id == null)
                $model->branch_id = Helper::getSelectedBranch();

            if(method_exists($model, 'generate_code') && $model->code === null)
                $model->code = $model->generate_code();
        });

        static::updating(function ($model) {
            $auth = App::make(Auth::class);
            $user_id = $auth->user()->id;

            if(in_array('updated_by', $model->autoFill))
                $model->updated_by = $user_id;
        });
    }
}
