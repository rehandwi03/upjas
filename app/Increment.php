<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Increment extends Model
{
    protected $guarded = [];
    protected $table = 'autoincrement';
    public $timestamps = false;
}
