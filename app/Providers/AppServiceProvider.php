<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Auth;
use Firebase\JWT\JWT;
use App\MSFarmer;
use App\MSUpja;
use App\MSAdmin;
use Illuminate\Support\Facades\Request;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Auth::class, function($app) {

            $token = Request::bearerToken();
            $credentials = JWT::decode($token, env('APP_KEY'), ['HS256']);
            $user = null;
            $role = null;

            if($credentials) {
                $role = $credentials->role;
                switch($role) {
                    case 'farmer':
                        $user = MSFarmer::find($credentials->id);
                    break;
                    case 'upja':
                        $user = MSUpja::find($credentials->id);
                    break;
                    case 'admin':
                        $user = MSAdmin::find($credentials->id);
                    break;
                }
                if($user) {
                    $user->id = $credentials->id;
                }
            }
            return new Auth([
                'user' => $user,
                'role' => $role,
            ]);
        });
    }
}
