<?php

namespace App;
use Illuminate\Support\Facades\App;
use App\Services\Auth;
use Illuminate\Database\Eloquent\Builder;

use Illuminate\Database\Eloquent\Model;

class FarmerAddress extends Model
{
    public $timestamps = false;
    protected $table = 'farmer_address';
    protected $primaryKey = 'id_faddress';
    protected $guarded = [];
    protected $hidden = ['id_farmer', 'id_province', 'id_district', 'id_subdistrict'];
    protected $fillable = ['id_village', 'faddress_name', 'faddress_desc', 'faddress_long', 'faddress_lat'];

    public static function boot()
    {
        $auth = App::make(Auth::class);

        parent::boot();

        static::addGlobalScope('role_scope', function (Builder $builder) use($auth) {
            $user_id = $auth->user()->id;

            switch($auth->role()) {
                case 'farmer':
                    $builder->where('id_farmer', $user_id);
                break;
            }
        });

        static::saving(function($model) use($auth) {
            $user_id = $auth->user()->id;

            if($auth->role() != 'admin')
                $model->id_farmer = $user_id;
        });
    }

    public function village()
    {
        return $this->belongsTo(MSVillage::class, 'id_village');
    }
}
