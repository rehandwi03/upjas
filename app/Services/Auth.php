<?php

namespace App\Services;

class Auth
{

    private $user;
    private $role;

    public function __construct($arr)
    {
        $this->role = $arr['role'];
        $this->user = $arr['user'];
    }

    public function user()
    {
        return $this->user;
    }

    public function role()
    {
        return $this->role;
    }
}
