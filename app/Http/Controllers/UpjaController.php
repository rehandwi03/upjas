<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\MSUpja;
use App\Models\TransMaster;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\App;
use App\Services\Auth;

class UpjaController extends Controller
{
    public $types = [
        'ot' => 'olahTanah',
        'jp' => 'jasaPanen',
        'jt' => 'jasaTanam',
        'irigasi' => 'irigasi',
        'jb' => 'jualBenih',
        'jg' => 'jualGabah',
        'milling' => 'milling',
    ];

    public function index()
    {
        $upja = MSUpja::orderBy('upja_id', 'ASC')->get();
        $data = [
            "message" => "success",
            "data" => $upja
        ];
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id_province' => 'required|numeric',
            'id_district' => 'required|numeric',
            'id_subdistrict' => 'required|numeric',
            'id_village' => 'required|numeric',
            'upja_id' => 'required|string|max:10',
            'upja_fullname' => 'required|string|max:100',
            'upja_email' => 'required|string|max:50',
            'upja_phone' => 'required|string|max:15',
            'upja_password' => 'required|string',
            'upja_emergency_phone' => 'nullable|string|max:15',
            'upja_image' => 'required|string|max:100',
            'upja_path' => 'required|string|max:50',
            'upja_address' => 'required|string',
            'upja_lat' => 'required|string|max:20',
            'upja_long' => 'required|string|max:20',
        ]);

        try {
            $upja = MSUpja::firstOrCreate([
                'upja_id' => $request->upja_id
            ], [
                'id_province' => $request->id_province,
                'id_district' => $request->id_district,
                'id_subdistrict' => $request->id_subdistrict,
                'id_village' => $request->id_village,
                'upja_fullname' => $request->upja_fullname,
                'upja_email' => $request->upja_email,
                'upja_phone' => $request->upja_phone,
                'upja_password' => app('hash')->make($request->upja_password),
                'upja_emergency_phone' => $request->upja_emergency_phone,
                'upja_image' => $request->upja_image,
                'upja_path' => $request->upja_path,
                'upja_address' => $request->upja_address,
                'upja_lat' => $request->upja_lat,
                'upja_long' => $request->upja_long,
                'upja_status' => 'inactive',
                'upja_verified' => 0,
                'upja_hide' => 0,
            ]);
        } catch (\Exception $e) {
            $respon = [
                "message" => $e->getMessage(),
            ];
            return response()->json($respon, 400);
        }
        $respon = [
            "message" => "success"
        ];
        return response()->json($respon, 201);
    }

    public function update(Request $request)
    {
        $auth = App::make(Auth::class);
        $user_id = $auth->user()->id;

        $this->validate($request, [
            'id_village' => 'required|numeric',
            'upja_fullname' => 'required|string|max:100',
            'upja_email' => 'required|string|max:50',
            'upja_phone' => 'required|string|max:15',
            'upja_emergency_phone' => 'nullable|string|max:15',
            'upja_address' => 'required|string',
            'upja_lat' => 'required|string|max:20',
            'upja_long' => 'required|string|max:20',
        ]);

        $upja = MSUpja::find($user_id);
        $upja->fill($request->toArray());

        if($upja->save()) {
            $data = [
                "status" => "success",
                "message" => "Data berhasil diubah",
            ];
        } else {
            $data = [
                "status" => "failed",
                "message" => "Data gagal diubah",
            ];
        }

        return response()->json($data, 200);
    }

    public function performance(Request $request)
    {
        $auth = App::make(Auth::class);
        $user_id = $auth->user()->id;

        $type = $request->get('type');

        $data = [
            'total' => 0,
            'succeed' => 0,
            'percentage' => 0,
        ];

        try {
            $allTrans = TransMaster::select('id', 'status')->whereIn('status', ['finish', 'cancel']);

            switch($type) {
                case 'month':
                    $allTrans->whereMonth('date_approved', date('m'));
                    $allTrans->whereYear('date_approved', date('Y'));
                break;
                case 'year':
                    $allTrans->whereYear('date_approved', date('Y'));
                break;
            }

            $allTrans->get();
            $data['total'] = $allTrans->count();
            $data['succeed'] = $allTrans->where('status', 'finish')->count();
            if($data['total']) {
                $data['percentage'] = 100*$data['succeed']/$data['total'];
            }

        } catch (\Exception $e) {
            return response()->json([
                "message" => $e->getMessage()
            ], 400);
        }

        return response()->json(compact('data'), 200);
    }

    public function show(Request $request)
    {
        $auth = App::make(Auth::class);
        $user_id = $auth->user()->id;

        try {
            $upja = MSUpja::where('id_upja', '=', $user_id)->first();
        } catch (\Exception $e) {
            return response()->json([
                "message" => $e->getMessage()
            ], 400);
        }
        if($upja) {
            if($upja->upja_image) {
                $upja->image_url = url('upja/'.$upja->upja_image);
            } else {
                $upja->image_url = url('services/default.jpeg');
            }

            $upja->province_name = isset($upja->village->subdistrict->district->province)?$upja->village->subdistrict->district->province->province_name:null;
            $upja->province_id = isset($upja->village->subdistrict->district->province)?$upja->village->subdistrict->district->province->id_province:null;

            $upja->district_name = isset($upja->village->subdistrict->district)?$upja->village->subdistrict->district->district_name:null;
            $upja->district_id = isset($upja->village->subdistrict->district)?$upja->village->subdistrict->district->id_district:null;

            $upja->subdistrict_name = isset($upja->village->subdistrict)?$upja->village->subdistrict->subdistrict_name:null;
            $upja->subdistrict_id = isset($upja->village->subdistrict)?$upja->village->subdistrict->id_subdistrict:null;

            $upja->village_name = isset($upja->village)?$upja->village->village_name:null;
            $upja->village_id = isset($upja->village)?$upja->village->id_village:null;

            unset($upja->village);

            return response()->json([
                "status" => "success",
                "message" => "Berhasil menampilkan data upja",
                "data" => $upja
            ], 200);
        } else {
            return response()->json([
                "status" => "failed",
                "message" => "Data upja tidak ditemukan",
                "data" => null
            ], 200);
        }
    }

    public function upja_status(Request $request, $id)
    {
        $upja = MSUpja::findOrFail($id);
        try {
            $upja->update([
                'upja_status' => $request->upja_status
            ]);
        } catch (\Exception $e) {
            $respon = [
                "message" => $e->getMessage()
            ];
            return response()->json($respon, 400);
        }
        $respon = [
            "message" => "success"
        ];
        return response()->json($respon, 200);
    }

    public function upja_verified(Request $request, $id)
    {
        $upja = MSUpja::findOrFail($id);
        try {
            $upja->update([
                'upja_verified' => $request->upja_verified
            ]);
        } catch (\Throwable $th) {
            $respon = [
                "message" => "error"
            ];
            return response()->json($respon, 400);
        }
        $respon = [
            "message" => "success"
        ];
        return response()->json($respon, 200);
    }

    public function upja_hide(Request $request, $id)
    {
        $upja = MSUpja::findOrFail($id);
        try {
            $upja->update([
                'upja_hide' => $request->upja_hide
            ]);
        } catch (\Throwable $th) {
            $respon = ["message" => "error"];
            return response()->json($respon, 400);
        }
        $respon = [
            "message" => "success"
        ];
        return response()->json($respon, 200);
    }

    public function change_password(Request $request)
    {
        $this->validate($request, [
            'old_password' =>  'required',
            'new_password' =>  'required|confirmed|string',
        ]);

        try {
            $auth = App::make(Auth::class);
            $user_id = $auth->user()->id;

            $upja = MSUpja::where('id_upja', '=', $user_id)->first();
            if (Hash::check($request->old_password, $upja->upja_password)) {
                $upja->upja_password = app('hash')->make($request->new_password);
                if(!$upja->save()) {
                    return response()->json([
                        "status" => "failed",
                        "message" => "Password gagal diubah",
                    ], 200);
                }
            } else {
                return response()->json([
                    "status" => "failed",
                    "message" => "Password lama tidak sesuai",
                ], 200);
            }
        } catch (\Exception $e) {
            $respon = [
                "status" => "failed",
                "message" => $e->getMessage()
            ];
            return response()->json($respon, 400);
        }
        return response()->json([
            "status" => "success",
            "message" => "Password berhasil diubah",
        ], 200);
    }

    public function list(Request $request) {

        $data = MSUpja::select(['id_upja', 'id_village', 'upja_id', 'upja_fullname', 'upja_image', 'upja_address', 'upja_lat', 'upja_long'])->with([
            'village.subdistrict.district.province'
        ]);

        if(isset($request->type)) {
            $data->has($this->types[$request->type]);
        }

        if(isset($request->id_village)) {
            $data->whereHas('village', function($q) use($request) {
                $q->where('id_village', $request->id_village);
            });
        };

        if(isset($request->id_subdistrict)) {
            $data->whereHas('village.subdistrict', function($q) use($request) {
                $q->where('id_subdistrict', $request->id_subdistrict);
            });
        };

        if(isset($request->id_district)) {
            $data->whereHas('village.subdistrict.district', function($q) use($request) {
                $q->where('id_district', $request->id_district);
            });
        };

        if(isset($request->id_province)) {
            $data->whereHas('village.subdistrict.district.province', function($q) use($request) {
                $q->where('id_province', $request->id_province);
            });
        };

        $data = $data->get();

        $data->transform(function($upja) {
            if($upja->upja_image) {
                $upja->image_url = url('upja/'.$upja->upja_image);
            } else {
                $upja->image_url = url('services/default.jpeg');
            }

            $upja->province_name = isset($upja->village->subdistrict->district->province)?$upja->village->subdistrict->district->province->province_name:null;
            $upja->province_id = isset($upja->village->subdistrict->district->province)?$upja->village->subdistrict->district->province->id_province:null;

            $upja->district_name = isset($upja->village->subdistrict->district)?$upja->village->subdistrict->district->district_name:null;
            $upja->district_id = isset($upja->village->subdistrict->district)?$upja->village->subdistrict->district->id_district:null;

            $upja->subdistrict_name = isset($upja->village->subdistrict)?$upja->village->subdistrict->subdistrict_name:null;
            $upja->subdistrict_id = isset($upja->village->subdistrict)?$upja->village->subdistrict->id_subdistrict:null;

            $upja->village_name = isset($upja->village)?$upja->village->village_name:null;
            $upja->village_id = isset($upja->village)?$upja->village->id_village:null;

            unset($upja->village);
            return $upja;
        });

        return response()->json(['data' => $data], 200);
    }
}
