<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\MSSubdistrict;
use Illuminate\Http\Request;

class SubdistrictController extends Controller
{
    public function index($id_district)
    {
        $sub = MSSubdistrict::select('id_subdistrict', 'subdistrict_name')->orderBy('subdistrict_name');
        if($id_district) {
            $sub->where('id_district', $id_district);
        }
        $sub = $sub->get();
        $respon = [
            "message" => "success",
            "data" => $sub
        ];
        return response()->json($respon, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id_district' => 'required|string|numeric',
            'id_province' => 'required|string|numeric',
            'subdistrict_name' => 'required|string|max:100'
        ]);
        try {
            $sub = MSSubdistrict::firstOrCreate([
                'subdistrict_name' => $request->subdistrict_name,
            ], [
                'id_district' => $request->id_district,
                'id_province' => $request->id_province,
                'subdistrict_hide' => 0
            ]);
        } catch (\Throwable $th) {
            $respon = ["message" => "error"];
            return response()->json($respon, 400);
        }
        $respon = [
            "message" => "success"
        ];
        return response()->json($respon, 201);
    }

    public function subdistrict_hide(Request $request, $id)
    {
        $this->validate($request, [
            'subdistrict_hide' => 'required|string|numeric',
        ]);

        $sub = MSSubdistrict::findOrFail($id);
        try {
            $sub->update([
                'subdistrict_hide' => $request->subdistrict_hide
            ]);
        } catch (\Throwable $th) {
            $respon = [
                "message" => "error"
            ];
            return response()->json($respon, 400);
        }
        $respon = ["message" => "success"];
        return response()->json($respon, 200);
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'id_district' => 'nullable|string|numeric',
            'id_province' => 'nullable|string|numeric',
            'subdistrict_name' => 'nullable|string|max:100'
        ]);

        $sub = MSSubdistrict::findOrFail($id);

        try {
            $sub->update([
                'subdistrict_name' => $request->subdistrict_name,
                'id_district' => $request->id_district,
                'id_province' => $request->id_province
            ]);
        } catch (\Throwable $th) {
            $respon = [
                "message" => "error"
            ];
            return response()->json($respon, 400);
        }
        $respon = ["message" => "success"];
        return response()->json($respon, 200);
    }

    public function id_district($id)
    {
        try {
            $sub = MSSubdistrict::orderBy('subdistrict_name', 'ASC')->select('subdistrict_name')->where('id_district', '=', $id)->get();
        } catch (\Exception $e) {
            $respon = [
                "message" => $e->getMessage()
            ];
            return response()->json($respon, 400);
        }
        return response()->json([
            "status" => "success",
            "data"  => $sub
        ], 200);
    }
}
