<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\TransTillage;
use Illuminate\Http\Request;

class TransTillageController extends Controller
{
    public function index()
    {
        $tt = TransTillage::orderBy('id_ttillage', 'ASC')->get();
        return response()->json([
            "message" => "success",
            "data" => $tt
        ], 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id_trans' => 'required|numeric',
            'id_punit' => 'required|numeric',
            'id_ptrans' => 'required|numeric',
            'ttillage_count' => 'required|numeric',
            'ttillage_request_date' => 'required|date'
        ]);

        try {
            $tt = TransTillage::create([
                'id_trans' => $request->id_trans,
                'id_punit' => $request->id_punit,
                'id_ptrans' => $request->id_ptrans,
                'ttillage_count' => $request->ttillage_count,
                'ttillage_request_date' => $request->ttillage_request_date
            ]);
        } catch (\Exception $e) {
            return response()->json([
                "message" => $e->getMessage()
            ], 400);
        }
        return response()->json([
            "message" => "success"
        ], 200);
    }
}
