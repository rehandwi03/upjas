<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Response;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Str;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\App;
use App\Services\Auth;
use App\MSUpja;
use App\MSFarmer;

class FileController extends Controller
{

    public function __construct()
    {
    }

    public function upload(Request $request)
    {

        $data = [
            'status' => 'success',
            'message' => 'Upload Succeed',
            'file_name' => null
        ];

        $from = $request->get('to');
        $originalImage = $request->file('upload');

        $filename = Str::random(15).'.'.$originalImage->getClientOriginalExtension();

        $image = Image::make($originalImage);
        $fileUrl = null;

        switch($from) {
            case 'services':
                $auth = App::make(Auth::class);
                if($auth->role() != 'upja') {
                    return false;
                }

                $originalPath = public_path().'/services/';
                $image->fit(150,150);
                $image->save($originalPath.$filename);
                $fileUrl = url('services/'.$filename);
            break;
            case 'upja':
                $auth = App::make(Auth::class);
                if($auth->role() != 'upja') {
                    return false;
                }

                $originalPath = public_path().'/upja/';
                $image->fit(150,150);
                $image->save($originalPath.$filename);
                $fileUrl = url('upja/'.$filename);

                $model = MSUpja::find($auth->user()->id);
                $model->upja_image = $filename;
                $model->save();
            break;
            case 'farmer':
                $auth = App::make(Auth::class);
                if($auth->role() != 'farmer') {
                    return false;
                }

                $originalPath = public_path().'/farmer/';
                $image->fit(150,150);
                $image->save($originalPath.$filename);
                $fileUrl = url('farmer/'.$filename);

                $model = MSFarmer::find($auth->user()->id);
                $model->farmer_image = $filename;
                $model->save();
            break;
            default:
                $filename = null;
            break;
        }
        $data['file_name'] = $filename;
        $data['file_url'] = $fileUrl;

        return new JsonResponse(compact('data'));
    }
}
