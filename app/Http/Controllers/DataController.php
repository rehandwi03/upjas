<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request as HttpRequest;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Validator;

class DataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public $modelList = ['Varietas', 'MillingPackage', 'Implement', 'OlahTanah', 'Irigasi', 'JasaTanam', 'JasaPanen', 'JualGabah', 'JualBenih', 'Milling', 'UpjaUom', 'TransMaster', 'UpjaTransport', 'TransMasterActivity'];
    public $model = null;
    public $modelName = null;

    public function __construct()
    {
        $model = Request::route('model');
        if(!in_array($model, $this->modelList)) {
            $type = 'danger';
            $message = 'Model has no access to this API.';
            return new JsonResponse(compact('type', 'message'));
        }
        $this->model = 'App\Models\\'.$model;
        $this->modelName = $model;
    }

    public function get()
    {
        $data = $this->_getData();
        return new JsonResponse(compact('data'));
    }

    public function detail()
    {
        $id = Request::route('id');
        $data = $this->model::detail($id);
        return new JsonResponse(compact('data'));
    }

    public function getOptions()
    {
        $key_value = Request::get('value');
        $key_text = Request::get('text');

        $get_data = $this->model::getOptions();

        $data = [];
        foreach ($get_data as $row) {
            $text = [];
            foreach ($key_text as $a_key_text) {
                $text[] = $row->{$a_key_text};
            }
            $text = implode(' - ', $text);
            $data[] = [
                'value' => $row->{$key_value}, 'text' => $text
            ];
        }
        return new JsonResponse(compact('data'));
    }

    public function autocomplete($model)
    {
        $keyword = Request::get('keyword');
        $extra = Request::get('extra');

        $data = $this->model::getAutocomplete($keyword, $extra);
        return new JsonResponse($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function save(HttpRequest $request)
    {
        $model = $this->model;
        $errors = [];

        if($model::validationRules()) {
            $validator = Validator::make($request->all(), $model::validationRules());
            if ($validator->fails()) {
                $errors = $validator->errors()->all();
                return new JsonResponse(compact('errors'));
            }
        }

        $data = Request::get('data');

        $selectedData = false;
        if (isset($data['selectedData'])) {
            $selectedData = array_keys(array_filter($data['selectedData'], function($v) {
                return $v;
            }));
        }

        $new = null;

        $message = '';
        $type = 'warning';
        $closePopup = false;
        $clearValue = false;
        if($selectedData) {
            $getSelectedData = $model::whereIn('id', $selectedData)->get();
            $message = 'Data berhasil diubah.';
            $closePopup = true;
            unset($data['selectedData']);
            $clearValue = true;
        } else if(isset($data['id'])) {
            $id = $data['id'];
            $new = $model::find($id);
            $message = 'Data berhasil diubah.';
            $closePopup = true;
            $clearValue = true;
        } else {
            switch($model) {
                default:
                    $new = new $model;
                break;
            }
            $message = 'Data berhasil ditambahkan.';
            $clearValue = true;
        }
        $isSaved = true;
        if($selectedData) {
            foreach($getSelectedData as $new) {
                foreach ($data as $key => $value) {
                    if($value == null) {
                        unset($data[$key]);
                    }
                }

                $new->fill($data);
                if(!$new->save()) {
                    $isSaved = false;
                    break;
                }
            }
        } else {
            $new->fill($data);
            $isSaved = $new->save();
        }
        if(!$isSaved) {
            $errors = $new->getErrors();
            $message = 'Terjadi kesalahan.';
            $clearValue = false;
            $closePopup = false;
        } else {
            $type = 'success';
        }
        if($errors) {
            $message = null;
        }
        $data = null;
        switch($model) {
            case 'App\Models\Sales':
                $data = $model::detail($new->id);
            break;
        }
        return new JsonResponse(compact('data','type','message','closePopup','clearValue','errors'));
    }

    public function export()
    {
        return $this->model::export();
    }

    public function import(HttpRequest $request)
    {
        ini_set('max_execution_time', 7200);

        $file = $request->file('upload');

        $data = $this->model::import($file);

        return new JsonResponse($data);
    }

    public function delete()
    {
        $loopMassDelete = [];
        $model = $this->model;
        $id = Request::get('id');
        $selectedData = Request::get('selectedData');
        if($selectedData) {
            $selectedData = array_keys(array_filter($selectedData, function($v) {
                return $v;
            }));

            $obj = $model::whereIn('id', $selectedData);

            if(in_array($this->modelName, $loopMassDelete)) {
                $isFailed = false;
                $isSucceed = false;
                foreach($obj->get() as $detailObject) {
                    if($detailObject->delete()) {
                        $isSucceed = true;
                    } else {
                        $isFailed = true;
                    }
                }
                if($isSucceed) {
                    $message = 'Data berhasil dihapus.';
                    $type = 'success';
                } else {
                    $message = 'Terjadi kesalahan.';
                    $type = 'warning';
                }
            } else {
                if(!$obj->delete()) {
                    $message = 'Terjadi kesalahan.';
                    $type = 'warning';
                } else {
                    $message = 'Data berhasil dihapus.';
                    $type = 'success';
                }
            }
        } else {
            $obj = $model::find($id);

            if(!$obj) {
                $message = 'Data tidak ditemukan.';
                $type = 'warning';
            } else {
                if(!$obj->delete()) {
                    $message = 'Terjadi kesalahan.';
                    $type = 'warning';
                } else {
                    $message = 'Data berhasil dihapus.';
                    $type = 'success';
                }
            }
        }

        //$data = $this->_getData();
        return new JsonResponse(compact('message', 'type'));
    }

    public function changeValue()
    {
        $model = $this->model;
        $data = Request::get('data');
        $success = false;
        $reload = false;

        $message = [
            'message' => 'Data berhasil diubah',
            'type' => 'success',
        ];

        $obj = $model::find($data['id']);

        if(!$obj) {
            $message['message'] = 'Data tidak ditemukan.';
            $message['type'] = 'warning';
        }

        $obj->fill([$data['column'] => $data['value']]);

        if(!$obj->save()) {
            $message['message'] = 'Terjadi kesalahan.';
            $message['type'] = 'warning';
        } else {
            $success = true;
        }

        return new JsonResponse(compact('reload','message', 'success'));
    }

    private function _getData()
    {
        return $this->model::dataviewsAll();
    }
}
