<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\MSAdmin;
use App\MSFarmer;
use App\MSUpja;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'username' => 'nullable|string',
            'password' => 'required|string',
            'farmer_phone' => 'nullable|string',
            'upja_phone' => 'nullable|string'
        ]);

        if ($request->has('farmer_phone')) {
            return $this->login_farmer($request->farmer_phone, $request->password);
        }
        if ($request->has('username')) {
            return $this->login_admin($request->username, $request->password);
        }
        if ($request->has('upja_phone')) {
            return $this->login_upja($request->upja_phone, $request->password);
        }
    }

    private function login_admin($username, $password)
    {
        $admin = MSAdmin::with('role')->where('admin_username', '=', $username)->first();
        if (!$admin) {
            $response = [
                "message" => "username dan password tidak sesuai",
                "result" => [
                    "token" => null
                ]
            ];
            return response()->json($response, 401);
        }
        if (Hash::check($password, $admin->admin_password)) {
            $payload = [
                'iss' => "lumen-jwt", // Issuer of the token
                'id' => $admin->id_admin, // Subject of the token
                'role' => "admin",
                'iat' => time(), // Time when JWT was issued.
                //'exp' => time() + 60 * 60 // Expiration time
            ];
            $respon = [
                "message" => "login berhasil",
                "result" => [
                    "token" => JWT::encode($payload, env('APP_KEY'))
                ]
            ];
            return response()->json($respon, 200);
        } else {
            $respon = [
                "message" => "login gagal",
                "result" => [
                    "token" => null
                ]
            ];
            return response()->json($respon, 401);
        }
    }

    private function login_upja($phone, $password)
    {
        $upja = MSUpja::where(
            [
                ['upja_phone', '=', $phone],
                ['upja_status', '=', 'active'],
                ['upja_verified', '=', 1],
                ['upja_hide', '=', 0]
            ]
        )->first();
        // dd($upja);
        if (!$upja) {
            $response = [
                "message" => "nomor telepon dan password tidak sesuai",
                "result" => [
                    "token" => null
                ]
            ];
            return response()->json($response, 401);
        }
        if (Hash::check($password, $upja->upja_password)) {
            $payload = [
                'iss' => "lumen-jwt", // Issuer of the token
                'id' => $upja->id_upja, // Subject of the token
                'role' => "upja", // Subject of the token
                'iat' => time(), // Time when JWT was issued.
                //'exp' => time() + 60 * 60 // Expiration time
            ];

            if($upja->upja_image) {
                $upja->image_url = url('upja/'.$upja->upja_image);
            } else {
                $upja->image_url = url('services/default.jpeg');
            }

            $upja->province_name = isset($upja->village->subdistrict->district->province)?$upja->village->subdistrict->district->province->province_name:null;
            $upja->province_id = isset($upja->village->subdistrict->district->province)?$upja->village->subdistrict->district->province->id_province:null;

            $upja->district_name = isset($upja->village->subdistrict->district)?$upja->village->subdistrict->district->district_name:null;
            $upja->district_id = isset($upja->village->subdistrict->district)?$upja->village->subdistrict->district->id_district:null;

            $upja->subdistrict_name = isset($upja->village->subdistrict)?$upja->village->subdistrict->subdistrict_name:null;
            $upja->subdistrict_id = isset($upja->village->subdistrict)?$upja->village->subdistrict->id_subdistrict:null;

            $upja->village_name = isset($upja->village)?$upja->village->village_name:null;
            $upja->village_id = isset($upja->village)?$upja->village->id_village:null;

            unset($upja->village);

            $respon = [
                "message" => "login berhasil",
                "result" => [
                    "data" => $upja,
                    "token" => JWT::encode($payload, env('APP_KEY'))
                ]
            ];
            return response()->json($respon, 200);
        } else {
            $respon = [
                "message" => "login gagal",
                "result" => [
                    "token" => null
                ]
            ];
            return response()->json($respon, 401);
        }
    }

    private function login_farmer($phone, $password)
    {
        $farmer = MSFarmer::where(
            [
                ['farmer_phone', '=', $phone],
                ['farmer_status', '=', 'active'],
                ['farmer_hide', '=', 0]
            ]
        )->first();
        if (!$farmer) {
            $response = [
                "message" => "nomor telepon dan password tidak sesuai",
                "result" => [
                    "token" => null
                ]
            ];
            return response()->json($response, 401);
        }
        if (Hash::check($password, $farmer->farmer_password)) {
            $payload = [
                'iss' => "lumen-jwt", // Issuer of the token
                'id' => $farmer->id_farmer, // Subject of the token
                'role' => "farmer", // Subject of the token
                'iat' => time(), // Time when JWT was issued.
                //'exp' => time() + 60 * 60 // Expiration time
            ];
            if($farmer->farmer_image) {
                $farmer->image_url = url('farmer/'.$farmer->farmer_image);
            } else {
                $farmer->image_url = url('services/default.jpeg');
            }
            $respon = [
                "message" => "login berhasil",
                "result" => [
                    "data" => [
                        'fullname' => $farmer->farmer_fullname,
                        'email' => $farmer->farmer_email,
                        'phone' => $farmer->farmer_phone,
                        'status' => $farmer->farmer_status,
                        'verified' => $farmer->farmer_verified,
                        "image_url" => $farmer->image_url
                    ],
                    "token" => JWT::encode($payload, env('APP_KEY'))
                ]
            ];
            return response()->json($respon, 200);
        } else {
            $respon = [
                "message" => "login gagal",
                "result" => [
                    "token" => null
                ]
            ];
            return response()->json($respon, 401);
        }
    }
}
