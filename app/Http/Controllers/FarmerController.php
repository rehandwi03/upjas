<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\MSFarmer;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Increment;
use Illuminate\Support\Facades\App;
use App\Services\Auth;

class FarmerController extends Controller
{
    public function index()
    {
        $farmer = MSFarmer::orderBy('id_farmer', 'ASC')->get();
        $data = [
            "status" => "success",
            "data" => $farmer
        ];
        return response()->json($data, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'farmer_phone' => 'required|string|unique:ms_farmer|max:15',
            'farmer_email' => 'nullable|email|unique:ms_farmer',
            'farmer_fullname' => 'required|string',
            'farmer_password' => 'required|string',
        ]);

        $increment = Increment::firstOrCreate([
            'model' => 'Farmer',
        ], [
            'last_number' => 1,
        ]);

        $farmer = MSFarmer::firstOrCreate([
            'farmer_phone' => $request->farmer_phone,
        ], [
            'farmer_id' => 'FRM'.substr(date('Y'), 2).str_pad($increment->last_number, 4, 0, STR_PAD_LEFT),
            'farmer_email' => $request->farmer_email,
            'farmer_fullname' => $request->farmer_fullname,
            'farmer_password' =>  app('hash')->make($request->farmer_password),
            'farmer_status' => 'active',
            'farmer_verified' => 0,
            'farmer_verify_code' => 0,
            'farmer_hide' => 0,
        ]);

        if($farmer) {
            $increment = Increment::where('model', 'Farmer')->first();
            $increment->last_number++;
            $increment->save();

            $payload = [
                'iss' => "lumen-jwt", // Issuer of the token
                'id' => $farmer->id_farmer, // Subject of the token
                'role' => "farmer",
                'iat' => time(), // Time when JWT was issued.
                //'exp' => time() + 60 * 60 // Expiration time
            ];
            $data = [
                "status" => "success",
                "message" => "Berhasil registrasi",
                "token" => JWT::encode($payload, env('APP_KEY'))
            ];
        } else {
            $data = [
                "status" => "success",
                "message" => "Gagal registrasi",
                "token" => null
            ];
        }

        return response()->json($data, 201);
    }

    public function update(Request $request)
    {
        $auth = App::make(Auth::class);
        $user_id = $auth->user()->id;

        $this->validate($request, [
            'farmer_email' => 'nullable|email|unique:ms_farmer,farmer_email,'.$user_id.',id_farmer',
            //'farmer_phone' => 'required|string|numeric|unique:ms_farmer,farmer_phone,'.$user_id.',id_farmer|max:15',
            'farmer_fullname' => 'required|string',
        ]);

        $farmer = MSFarmer::find($user_id);

        $farmer->farmer_fullname = $request->farmer_fullname;
        $farmer->farmer_phone = $request->farmer_phone;
        $farmer->farmer_email = $request->farmer_email;

        if($farmer->save()) {
            $data = [
                "status" => "success",
                "message" => "Data berhasil diubah",
            ];
        } else {
            $data = [
                "status" => "failed",
                "message" => "Data gagal diubah",
            ];
        }

        return response()->json($data, 200);
    }

    public function farmer_status(Request $request, $id)
    {
        // dd($request);
        $this->validate($request, [
            'farmer_status' => 'required|string'
        ]);
        $farmer = MSFarmer::findOrFail($id);
        try {
            $farmer->update([
                'farmer_status' => $request->farmer_status
            ]);
        } catch (\Exception $e) {
            $respon = [
                "status" => $e->getMessage(),
                "message" => "can't change status"
            ];
            return response()->json($respon, 400);
        }
        $respon = [
            "status" => "success",
            "message" => "Berhasil mengubah status petani"
        ];
        return response()->json($respon, 200);
    }

    public function farmer_hide(Request $request, $id)
    {
        $this->validate($request, [
            'farmer_hide' => 'required|string|numeric'
        ]);

        $farmer = MSFarmer::findOrFail($id);
        try {
            $farmer->update([
                'farmer_hide' => $request->farmer_hide
            ]);
        } catch (\Exception $e) {
            $respon = [
                "message" => "can't change hide code"
            ];
            return response()->json($respon, 400);
        }
        $respon = [
            "message" => "success"
        ];
        return response()->json($respon, 200);
    }

    public function verify_code(Request $request, $id)
    {
        $this->validate($request, [
            'farmer_verify_code' => 'required|string'
        ]);

        $farmer = MSFarmer::findOrFail($id);
        try {
            $farmer->update([
                'farmer_verify_code' => $request->farmer_verify_code
            ]);
        } catch (\Exception $e) {
            $respon = [
                "message" => "can't change verify code"
            ];
            return response()->json($respon, 400);
        }
        $respon = [
            "message" => "success"
        ];
        return response()->json($respon, 200);
    }

    public function show(Request $request)
    {
        $auth = App::make(Auth::class);
        $user_id = $auth->user()->id;

        try {
            $farmer = MSFarmer::where('id_farmer', '=', $user_id)->first();
        } catch (\Exception $e) {
            return response()->json([
                "message" => $e->getMessage()
            ], 400);
        }
        if($farmer) {
            return response()->json([
                "status" => "success",
                "message" => "Berhasil menampilkan data petani",
                "data" => $farmer
            ], 200);
        } else {
            return response()->json([
                "status" => "failed",
                "message" => "Data petani tidak ditemukan",
                "data" => null
            ], 200);
        }
    }

    public function change_password(Request $request)
    {
        $this->validate($request, [
            'old_password' =>  'required',
            'new_password' =>  'required|confirmed|string',
        ]);

        try {
            $auth = App::make(Auth::class);
            $user_id = $auth->user()->id;

            $farmer = MSFarmer::where('id_farmer', '=', $user_id)->first();
            if (Hash::check($request->old_password, $farmer->farmer_password)) {
                $farmer->farmer_password = app('hash')->make($request->new_password);
                if(!$farmer->save()) {
                    return response()->json([
                        "status" => "failed",
                        "message" => "Password gagal diubah",
                    ], 200);
                }
            } else {
                return response()->json([
                    "status" => "failed",
                    "message" => "Password lama tidak sesuai",
                ], 200);
            }
        } catch (\Exception $e) {
            $respon = [
                "status" => "failed",
                "message" => $e->getMessage()
            ];
            return response()->json($respon, 400);
        }
        return response()->json([
            "status" => "success",
            "message" => "Password berhasil diubah",
        ], 200);
    }
}
