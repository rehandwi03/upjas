<?php

namespace App\Http\Controllers;

use App\FarmerAddress;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use App\Services\Auth;

class FAddressController extends Controller
{
    public function index()
    {
        try {
            $address = FarmerAddress::with('village.subdistrict.district.province')->orderBy('id_faddress', 'ASC')->get();

            $address->transform(function($item) {
                $item->province_name = isset($item->village->subdistrict->district->province)?$item->village->subdistrict->district->province->province_name:null;
                $item->province_id = isset($item->village->subdistrict->district->province)?$item->village->subdistrict->district->province->id_province:null;

                $item->district_name = isset($item->village->subdistrict->district)?$item->village->subdistrict->district->district_name:null;
                $item->district_id = isset($item->village->subdistrict->district)?$item->village->subdistrict->district->id_district:null;

                $item->subdistrict_name = isset($item->village->subdistrict)?$item->village->subdistrict->subdistrict_name:null;
                $item->subdistrict_id = isset($item->village->subdistrict)?$item->village->subdistrict->id_subdistrict:null;

                $item->village_name = isset($item->village)?$item->village->village_name:null;
                $item->village_id = isset($item->village)?$item->village->id_village:null;

                unset($item->village);
                return $item;
            });
        } catch (\Exception $e) {
            $respon = ["message" => $e->getMessage()];
        }

        $respon = ["message" => "succes", "data" => $address];
        return response()->json($respon, 200);
    }

    public function delete(Request $request)
    {
        try {
            $address = FarmerAddress::find($request->id_faddress)->delete();
        } catch (\Exception $e) {
            $respon = ["message" => $e->getMessage()];
            return response()->json($respon, 400);
        }
        $respon = ["message" => "success"];
        return response()->json($respon, 200);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'id_village' => 'required|string|numeric',
            'faddress_name' => 'required|string|max:100',
            'faddress_desc' => 'required|string',
            'faddress_long' => 'required|string',
            'faddress_lat' => 'required|string'
        ]);

        $auth = App::make(Auth::class);
        $user_id = $auth->user()->id;

        try {
            if(isset($request->id_faddress)) {
                $address = FarmerAddress::find($request->id_faddress);
            } else {
                $address = new FarmerAddress;
            }
            $address->fill([
                'id_village' => $request->id_village,
                'faddress_name' => $request->faddress_name,
                'faddress_desc' => $request->faddress_desc,
                'faddress_long' => $request->faddress_long,
                'faddress_lat' => $request->faddress_lat,
            ]);
            $address->save();
        } catch (\Exception $e) {
            $respon = ["message" => $e->getMessage()];
            return response()->json($respon, 400);
        }
        $respon = ["status" => "success", "message" => 'Data berhasil diubah'];
        return response()->json($respon, 201);
    }

    public function id_farmer(Request $request)
    {
        try {
            $addres = FarmerAddress::with('province', 'province.district', 'province.district.subdistrict', 'province.district.subdistrict.village')->where('id_farmer', '=', $request->id)->first();
        } catch (\Exception $e) {
            $respon = ["message" => $e->getMessage()];
            return response()->json($respon, 400);
        }
        // dd($addres);
        $respon = [
            "message" => "success",
            "data" => [
                "faddress_name" => $addres->faddress_name,
                "faddress_desc" => $addres->faddress_desc,
                "faddress_long" => $addres->faddress_long,
                "faddress_lat" => $addres->faddress_lat,
                "province" => $addres->province->province_name,
                "district" => $addres->province->district->district_name,
                "subdisrtict" => $addres->province->district->subdistrict->subdistrict_name,
                "village" => $addres->province->district->subdistrict->village->village_name,

            ]
        ];
        return response()->json($respon, 200);
    }
}
