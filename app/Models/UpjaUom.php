<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use App\Models\BaseModel;
use App\Traits\ModelObserver;
use Illuminate\Support\Facades\App;
use App\Services\Auth;

class UpjaUom extends BaseModel
{
    use ModelObserver;

    protected $table = 'upja_uom';
    protected $fillable = ['id_upja', 'uom_name', 'uom_desc', 'uom_hide'];
    protected $autoFill = [];
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();

        static::saving(function($model) {
            $auth = App::make(Auth::class);

            if($auth) {
                switch($auth->role()) {
                    case 'upja':
                        $model->id_upja = $auth->user()->id;
                    break;
                }
            } else {
                return false;
            }
        });

        static::creating(function($model) {
            $model->uom_created = date('Y-m-d H:i:s');
        });

        static::updated(function($model) {
            $model->uom_updated = date('Y-m-d H:i:s');
        });
    }

    public static function dataviewsAll()
    {
        $per_page = self::$per_page;

        $filter = Request::get('filter')?:[];

        $data = self::select('*');

        $data = $data->paginate($per_page);

        return $data;
    }

}
