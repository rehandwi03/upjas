<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use App\Models\BaseModel;
use App\Traits\ModelObserver;
use App\FarmerAddress;
use Illuminate\Support\Facades\App;
use App\Services\Auth;

class JualGabah extends BaseModel
{
    use ModelObserver;

    protected $table = 'ms_jual_gabah';
    protected $fillable = ['id_upja', 'image', 'type', 'price', 'terms', 'order_min', 'is_publish', 'is_hide', 'uom_list', 'uom_default'];
    protected $autoFill = ['created_by', 'id_upja', 'created_by_role'];

    public static function boot()
    {
        parent::boot();

    }

    public static function dataviewsAll()
    {
        $per_page = self::$per_page;

        $filter = Request::get('filter')?:[];

        $data = self::select('*');

        $data = $data->paginate($per_page);

        return $data;
    }

}
