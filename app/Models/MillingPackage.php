<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use App\Models\BaseModel;
use App\Traits\ModelObserver;

class MillingPackage extends BaseModel
{
    use ModelObserver;

    protected $table = 'milling_packing';
    protected $fillable = ['id_milling', 'type', 'price', 'is_hide', 'uom', 'quantity'];
    protected $autoFill = ['created_by'];

    public static function boot()
    {
        parent::boot();

    }

    public function satuan()
    {
        return $this->belongsTo('App\Models\UpjaUom', 'uom');
    }

    public static function dataviewsAll()
    {
        $per_page = self::$per_page;

        $filter = Request::get('filter')?:[];

        $data = self::select('*');

        $data = $data->paginate($per_page);

        return $data;
    }

}
