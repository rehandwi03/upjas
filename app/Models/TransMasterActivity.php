<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use App\Traits\ModelObserver;
use Illuminate\Support\Facades\App;
use App\Services\Auth;
use Illuminate\Database\Eloquent\Builder;

class TransMasterActivity extends BaseModel
{
    use ModelObserver;

    protected $table = 'tr_master_activity';
    protected $fillable = ['id_tr_master', 'status', 'is_done', 'description'];
    protected $autoFill = ['created_by', 'created_by_role'];

    public static function boot()
    {
        $auth = App::make(Auth::class);

        parent::boot();

        static::addGlobalScope('role_scope', function (Builder $builder) use($auth) {
            $user_id = $auth->user()->id;

            switch($auth->role()) {
                case 'farmer':
                    $builder->whereHas('transMaster', function($q) use ($user_id) {
                        $q->where('id_farmer', $user_id);
                    });
                break;
                case 'upja':
                    $builder->whereHas('transMaster', function($q) use ($user_id) {
                        $q->where('id_upja', $user_id);
                    });
                break;
            }
        });

        static::deleting(function($model) use($auth) {
            if($auth->role() == $model->created_by_role && $auth->user()->id == $model->created_by) {
                return true;
            } else {
                return false;
            }
        });

        static::deleted(function($model) use($auth) {
            $lastStatus = self::where('id_tr_master', $model->id_tr_master)->orderBy('id', 'desc')->first();

            if($lastStatus) {
                $model->transMaster->status = $lastStatus->status;
                $model->transMaster->cancel_reason = null;
                if($lastStatus->status == 'accept') {
                    $model->transMaster->is_approved = true;
                    $model->transMaster->date_approved = date('Y-m-d', strtotime($lastStatus->created_at));
                } else if($lastStatus->status == 'cancel') {
                    $model->transMaster->cancel_reason = $lastStatus->description;
                }
            } else {
                $model->transMaster->status = 'waiting';
            }
            $model->transMaster->save();
        });

        static::updating(function($model) use($auth) {
            $model->status = $model->getOriginal('status');
            $model->id_tr_master = $model->getOriginal('id_tr_master');
        });

        static::saving(function($model) use($auth) {
            if($auth->role() == 'farmer') {
                if(!in_array($model->status, ['cancel'])) {
                    return false;
                }
            }

            if($model->status == 'accept') {
                $model->description = '';
                $model->is_done = 1;
            } else if($model->status == 'finish') {
                $model->description = '';
                $model->is_done = 1;
            }  else if($model->status == 'cancel') {
                $model->is_done = 1;
            }

            if($model->is_done != $model->getOriginal('is_done')) {
                if($model->is_done == 1) {
                    $model->done_at = date('Y-m-d H:i:s');
                } else {
                    $model->done_at = null;
                }
            }
        });

        static::created(function($model) use($auth) {

            $model->transMaster->status = $model->status;
            if($model->status == 'accept') {
                $model->transMaster->is_approved = true;
                $model->transMaster->date_approved = date('Y-m-d');
            } else if($model->status == 'cancel') {
                $model->transMaster->cancel_reason = $model->description;
            }
            $model->transMaster->save();
        });
    }

    public function transMaster()
    {
        return $this->belongsTo('App\Models\TransMaster', 'id_tr_master');
    }

    public static function dataviewsAll()
    {
        $per_page = self::$per_page;

        $id = Request::get('id')?:[];

        $data = self::select('id', 'status', 'is_done', 'done_at', 'description', 'created_at')->orderBy('created_at', 'asc');

        if($id) {
            $data->where('id_tr_master', $id);
        } else {
            return null;
        }
        $data = $data->paginate($per_page);

        $data->getCollection()->transform(function ($item) {
            if($item->done_at) {
                $item->date = date('l, d M Y', strtotime($item->done_at));
                $item->time = date('H:i', strtotime($item->done_at));
            } else {
                $item->date = null;
                $item->time = null;
            }

            switch($item->status) {
                case 'accept':
                    $item->description = 'Transaksi diterima oleh peyedia jasa';
                break;
                case 'finish':
                    $item->description = 'Transaksi telah selesai';
                break;
            }
            return $item;
        });

        return $data;
    }

}
