<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use App\Services\Auth;

abstract class BaseModel extends Model
{

    protected $autoFill = [];
    protected $errors = [];
    protected $fillValue = [];
    protected static $per_page = 30;

    protected static function boot()
    {
        parent::boot();

        static::saving(function($model) {
            $auth = App::make(Auth::class);

            if(isset($model->roles[$auth->role()])) {
                $unsets = array_diff($model->fillable, $model->roles[$auth->role()]);

                foreach($unsets as $unset) {
                    unset($model->$unset);
                }
            }
        });
    }

    public static function import($file)
    {
        return ['success' => false, 'messages' => [['message' => 'Import data is not supported in this page', 'type' => 'error']]];
    }

    public static function export()
    {
        return null;
    }

    public static function dataviewsAll()
    {
        $per_page = self::$per_page;

        return self::orderBy('created_at', 'desc')->paginate($per_page);
    }

    public static function detail($id)
    {
        $data = self::find($id);

        return $data;
    }

    public static function getOptions()
    {
        return self::all();
    }

    public static function getAutocomplete($keyword, $extra, $limit = 10)
    {
        return [];
    }

    public function fill(array $attributes)
    {
        parent::fill($attributes);
        $this->fillValue = $attributes;
    }

    public static function validationRules()
    {
        return false;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public static function getConfigPerPage()
    {
        return self::$per_page;
    }
}
