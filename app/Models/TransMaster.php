<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use App\Models\BaseModel;
use App\Traits\ModelObserver;
use App\Models\TransDetail;
use App\Models\Implement;
use Illuminate\Support\Facades\App;
use App\Services\Auth;
use Illuminate\Database\Eloquent\Builder;

class TransMaster extends BaseModel
{
    use ModelObserver;

    protected $table = 'tr_master';
    protected $fillable = ['id_farmer', 'id_faddress', 'id_upja', 'id_service', 'type', 'dealing_date', 'other_address', 'payment_method'];
    protected $autoFill = ['created_by', 'id_farmer', 'created_by_role'];
    protected $roles = [
        'upja' => [],
        'farmer' => ['id_upja', 'id_faddress', 'id_service', 'type', 'dealing_date', 'other_address', 'payment_method'],
    ];
    protected static $typeList = [
        'ot' => ['name' => 'OlahTanah', 'fields' => ['id', 'title', 'seri', 'image', 'terms', 'uom_default']],
        'irigasi' => ['name' => 'Irigasi', 'fields' => ['id', 'title', 'seri', 'image', 'terms', 'uom_default']],
        'jt' => ['name' => 'JasaTanam', 'fields' => ['id', 'title', 'seri', 'image', 'type', 'terms', 'uom_default']],
        'jp' => ['name' => 'JasaPanen', 'fields' => ['id', 'title', 'seri', 'image', 'terms', 'uom_default']],
        'milling' => ['name' => 'Milling', 'fields' => ['id', 'title', 'image', 'terms', 'uom_default']],
        'jb' => ['name' => 'JualBenih', 'fields' => ['id', 'id_varietas', 'image', 'uom_default']],
        'jg' => ['name' => 'JualGabah', 'fields' => ['id', 'type', 'image', 'terms', 'uom_default']],
    ];

    public static function boot()
    {
        $auth = App::make(Auth::class);

        parent::boot();

        static::addGlobalScope('role_scope', function (Builder $builder) use($auth) {
            $user_id = $auth->user()->id;

            switch($auth->role()) {
                case 'farmer':
                    $builder->where('id_farmer', $user_id);
                break;
                case 'upja':
                    $builder->where('id_upja', $user_id);
                break;
            }
        });

        static::creating(function($model) {
            $value = $model->id_service;
            $modelName = self::$typeList[$model->type];

            if($modelName) {
                $modelClassName = 'App\Models\\'.$modelName['name'];
                $row = $modelClassName::with(['satuan' => function($q) { $q->select('id', 'uom_name', 'uom_desc'); }])->where('id', $value)->first();

                if(!$row) {
                    return false;
                }
            } else {
                return false;
            }

            if($model->faddress) {
                $model->id_village = $model->faddress->id_village;
                $model->address = $model->faddress->faddress_name;
            } else {
                return false;
            }
            $model->price_total = 0;
            $model->qty_total = 0;
            $model->is_other = 0;
            $model->is_paid = 0;
            $model->status = 'waiting';
            $model->cancel_reason = null;
        });


        static::saved(function($model) use($auth) {
            $oldStatus = $model->getOriginal('status');
            $type = $model->getOriginal('type');
            if($type && $model->status != $oldStatus) {
                $id_service = $model->getOriginal('id_service');

                $modelName = self::$typeList[$type];

                if($modelName) {
                    $modelName = 'App\Models\\'.$modelName['name'];
                    $row = $modelName::find($id_service);

                    if($row && isset($row->stock)) {
                        if($model->status == 'accept' && in_array($oldStatus, ['waiting','cancel','finish'])) {
                            $row->stock--;
                            $row->save();
                        } else if(($model->status == 'cancel' || $model->status == 'finish') && in_array($oldStatus, ['accept', 'process'])) {
                            $row->stock++;
                            $row->save();
                        }
                    }
                }
            };
        });

        static::created(function($model) {

            $quantity = @(float)$model->fillValue['quantity'];
            $totalPrice = 0;
            $totalQty = 0;

            if($quantity) {
                $value = $model->id_service;
                $modelName = self::$typeList[$model->type];

                if($modelName) {
                    $modelClassName = 'App\Models\\'.$modelName['name'];
                    $row = $modelClassName::with(['satuan' => function($q) { $q->select('id', 'uom_name', 'uom_desc'); }])->where('id', $value)->first();
                } else {
                    $row = [];
                }

                switch($modelName) {
                    case 'OlahTanah':
                    case 'JasaPanen':
                    case 'Milling':
                        $new = new TransDetail;
                        $new->id_tr_master = $model->id;
                        $new->is_master = true;
                        $new->price = $row->price;
                        $new->qty = $quantity;
                        $new->unit = $row->uom_default;
                        $new->total = $new->price*$new->qty;
                        $new->name = 'Sewa Mesin';
                        $new->save();

                        $totalPrice+= $new->total;
                        $totalQty+= $quantity;
                    break;
                    case 'Irigasi':
                        $new = new TransDetail;
                        $new->id_tr_master = $model->id;
                        $new->is_master = true;
                        $new->price = $row->gas_price;
                        $new->qty = $quantity;
                        $new->unit = $row->uom_default;
                        $new->total = $new->price*$new->qty;
                        $new->name = 'Biaya Bensin';
                        $new->save();

                        $totalPrice+= $new->total;
                        $totalQty+= $quantity;

                        $new = new TransDetail;
                        $new->id_tr_master = $model->id;
                        $new->is_master = true;
                        $new->price = $row->operator_price;
                        $new->qty = $quantity;
                        $new->unit = $row->uom_default;
                        $new->total = $new->price*$new->qty;
                        $new->name = 'Biaya Operator';
                        $new->save();

                        $totalPrice+= $new->total;
                        $totalQty+= $quantity;
                    break;
                }
            }

            $implements = @$model->fillValue['implements']?explode(',', $model->fillValue['implements']):[];

            if($implements) {
                $getImplements = $getImplements = Implement::whereIn('id', $implements)->get()->keyBy('id');

                foreach($implements as $implement) {
                    $new = new TransDetail;
                    $new->id_tr_master = $model->id;
                    $new->id_ot_implement = $implement;
                    $new->id_upja_transport = null;
                    $new->is_master = false;
                    $new->price = $getImplements[$implement]->price;
                    $new->qty = 1;
                    $new->total = $new->price*$new->qty;
                    $new->name = $getImplements[$implement]->name;
                    $new->save();

                    $totalPrice+= $new->total;
                    $totalQty+= $new->qty;
                }
            }

            $range = null;
            if($model->upja->village->id_village == $model->village->id_village) {
                $range = 'satu_desa';
            } else if($model->upja->village->subdistrict->id_subdistrict == $model->village->subdistrict->id_subdistrict) {
                $range = 'antar_desa';
            } else if($model->upja->village->subdistrict->district->id_district == $model->village->subdistrict->district->id_district) {
                $range = 'antar_kecamatan';
            } else if($model->upja->village->subdistrict->district->province->id_province == $model->village->subdistrict->district->province->id_province) {
                $range = 'antar_kota';
            }

            $getTransport = UpjaTransport::where('id_upja', $model->id_upja)->where('type', $model->type)->where('region', $range)->first();

            if($getTransport) {
                $new = new TransDetail;
                $new->id_tr_master = $model->id;
                $new->id_ot_implement = null;
                $new->id_upja_transport = $getTransport->id;
                $new->is_master = false;
                $new->price = $getTransport->price;
                $new->qty = 1;
                $new->total = $new->price*$new->qty;
                $new->name = 'Biaya Transport';
                $new->save();

                $totalPrice+= $new->total;
                $totalQty+= $new->qty;
            }

            $model->price_total = $totalPrice;
            $model->qty_total = $totalQty;
            $model->save();
        });
    }

    public function upja()
    {
        return $this->belongsTo('App\MSUpja', 'id_upja');
    }

    public function farmer()
    {
        return $this->belongsTo('App\MSFarmer', 'id_farmer');
    }

    public function village()
    {
        return $this->belongsTo('App\MSVillage', 'id_village');
    }

    public function faddress()
    {
        return $this->belongsTo('App\FarmerAddress', 'id_faddress');
    }

    public function details()
    {
        return $this->hasMany('App\Models\TransDetail', 'id_tr_master');
    }

    public static function dataviewsAll()
    {
        $per_page = self::$per_page;

        $limit = Request::get('limit')?:null;
        if($limit) {
            $per_page = $limit;
        }
        $detail = Request::get('detail');
        $filter = Request::get('filter')?:[];
        $status = Request::get('status');

        $data = self::with([
            'upja' => function($q) {
                $q->select('id_upja', 'upja_fullname', 'upja_address');
            },
            'farmer' => function($q) {
                $q->select('id_farmer', 'farmer_fullname');
            },
            'details.implement' => function($q) { $q->select('id', 'name'); },
            'details.transport' => function($q) { $q->select('id', 'region'); },
            'details.satuan' => function($q) { $q->select('id', 'uom_name'); },
            'village.subdistrict.district.province'
        ])->select('id', 'id_upja', 'id_farmer', 'id_service', 'id_village', 'address', 'type', 'price_total', 'dealing_date', 'date_approved', 'status', 'cancel_reason', 'payment_method')->orderBy('created_at', 'desc');

        if($status) {
            if($status == 'process') {
                $data->whereIn('status', ['process', 'accept', 'waiting']);
            } else if($status == 'history') {
                $data->whereIn('status', ['finish', 'cancel']);
            }
        }

        if($detail) {
            $data = $data->whereIn('id', [$detail])->get();
        } else {
            $data = $data->paginate($per_page);
        }

        $services = [];

        foreach($data as $dt) {
            $services[$dt->type][] = $dt->id_service;
        }

        $dataServices = [];

        foreach($services as $key => $values) {
            $modelName = self::$typeList[$key];

            if($modelName) {
                $modelClassName = 'App\Models\\'.$modelName['name'];
                $row = $modelClassName::with(['satuan' => function($q) { $q->select('id', 'uom_name', 'uom_desc'); }])->select($modelName['fields'])->whereIn('id', $values)->get()->keyBy('id');
            } else {
                $row = [];
            }
            $dataServices[$key] = $row;
        }

        $data->transform(function ($item) use($dataServices) {

            $item->province_name = isset($item->village->subdistrict->district->province)?$item->village->subdistrict->district->province->province_name:null;
            $item->province_id = isset($item->village->subdistrict->district->province)?$item->village->subdistrict->district->province->id_province:null;

            $item->district_name = isset($item->village->subdistrict->district)?$item->village->subdistrict->district->district_name:null;
            $item->district_id = isset($item->village->subdistrict->district)?$item->village->subdistrict->district->id_district:null;

            $item->subdistrict_name = isset($item->village->subdistrict)?$item->village->subdistrict->subdistrict_name:null;
            $item->subdistrict_id = isset($item->village->subdistrict)?$item->village->subdistrict->id_subdistrict:null;

            $item->village_name = isset($item->village)?$item->village->village_name:null;
            $item->village_id = isset($item->village)?$item->village->id_village:null;

            unset($item->village);

            if(isset($dataServices[$item->type][$item->id_service])) {
                $item->service = $dataServices[$item->type][$item->id_service];
                if($item->service->image) {
                    $item->service->image_url = url('services/'.$item->service->image);
                } else {
                    $item->service->image_url = url('services/default.jpeg');
                }
            } else {
                $item->service = null;
            }
            return $item;
        });

        if($detail && $data) {
            $data = $data[0];
        }

        return $data;
    }

}
