<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use App\Models\BaseModel;
use App\Traits\ModelObserver;

class UpjaTransport extends BaseModel
{
    use ModelObserver;

    protected $table = 'upja_transport';
    protected $fillable = ['id_upja', 'type', 'region', 'price'];
    protected $autoFill = ['created_by', 'id_upja', 'created_by_role'];

    public static function boot()
    {
        parent::boot();

    }

    public static function dataviewsAll()
    {
        $per_page = self::$per_page;

        $filter = Request::get('filter')?:[];

        $data = self::select('*');

        $data = $data->paginate($per_page);

        return $data;
    }

}
