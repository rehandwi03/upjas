<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use App\Models\BaseModel;
use App\Traits\ModelObserver;

class TransDetail extends BaseModel
{
    use ModelObserver;

    protected $table = 'tr_detail';
    protected $fillable = [];
    protected $autoFill = [];
    public $timestamps = false;

    public static function boot()
    {
        parent::boot();
    }

    public function transport() {
        return $this->hasOne('App\Models\UpjaTransport', 'id', 'id_upja_transport');
    }

    public function implement() {
        return $this->hasOne('App\Models\Implement', 'id', 'id_ot_implement');
    }

    public function satuan()
    {
        return $this->belongsTo('App\Models\UpjaUom', 'unit');
    }

    public static function dataviewsAll()
    {
        $per_page = self::$per_page;

        $filter = Request::get('filter')?:[];

        $data = self::select('*');

        $data = $data->paginate($per_page);

        return $data;
    }

}
