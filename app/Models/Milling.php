<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use App\Models\BaseModel;
use App\Traits\ModelObserver;
use App\FarmerAddress;
use Illuminate\Support\Facades\App;
use App\Services\Auth;
use Illuminate\Support\Facades\DB;

class Milling extends BaseModel
{
    use ModelObserver;

    protected $table = 'ms_milling';
    protected $fillable = ['id_upja', 'title', 'image', 'price', 'terms', 'order_min', 'is_publish', 'is_hide', 'uom_list', 'uom_default'];
    protected $autoFill = ['created_by', 'id_upja', 'created_by_role'];

    public static function boot()
    {
        parent::boot();

    }

    public function upja()
    {
        return $this->belongsTo('App\MSUpja', 'id_upja');
    }

    public function satuan()
    {
        return $this->belongsTo('App\Models\UpjaUom', 'uom_default');
    }

    public function transport()
    {
        return $this->hasMany('App\Models\UpjaTransport', 'id_upja', 'id_upja')->where('type', 'jp');
    }

    public function package()
    {
        return $this->hasMany('App\Models\MillingPackage', 'id_milling');
    }

    public static function dataviewsAll()
    {
        $per_page = self::$per_page;

        $id_upja = Request::get('id_upja');
        $detail = Request::get('detail');
        $id_faddress = Request::get('id_faddress');
        $filter = Request::get('filter')?:[];

        if($id_faddress) {
            $currentVillage = FarmerAddress::find($id_faddress)->village;
        } else {
            $currentVillage = false;
        }

        $data = self::with([
            'satuan' => function($q) { $q->select('id', 'uom_name', 'uom_desc'); },
            'transport' => function($q) { $q->select('id', 'id_upja', 'price', 'region'); },
            'package' => function($q) { $q->select('id', 'id_milling', 'type', 'uom', 'price', 'quantity'); },
            'upja' => function($q) {
                $q->select('id_upja', 'id_village', 'upja_fullname', 'upja_address', 'upja_long', 'upja_lat');
            },
            'upja.village.subdistrict.district.province'
            ])->select('*');

        if($id_upja) {
            $data->where('id_upja', $id_upja);
        }

        if($detail) {
            $data = $data->whereIn('id', [$detail])->get();
        } else {
            $data = $data->paginate($per_page);
        }

        // get automatic transport fare
        $auth = App::make(Auth::class);

        $data->transform(function ($item) use($currentVillage) {

            $range = null;

            if($currentVillage) {
                if($item->upja->village->id_village == $currentVillage->id_village) {
                    $range = 'satu_desa';
                } else if($item->upja->village->subdistrict->id_subdistrict == $currentVillage->subdistrict->id_subdistrict) {
                    $range = 'antar_desa';
                } else if($item->upja->village->subdistrict->district->id_district == $currentVillage->subdistrict->district->id_district) {
                    $range = 'antar_kecamatan';
                } else if($item->upja->village->subdistrict->district->province->id_province == $currentVillage->subdistrict->district->province->id_province) {
                    $range = 'antar_kota';
                }
            }

            if($range) {
                $item->current_transport = array_filter($item->transport->toArray(), function($item) use($range) {
                    return $item['region'] == $range;
                });
                if($item->current_transport) {
                    $item->current_transport = $item->current_transport[0];
                }
            } else {
                $item->current_transport = null;
            }

            $item->upja->province_name = isset($item->upja->village->subdistrict->district->province)?$item->upja->village->subdistrict->district->province->province_name:null;
            $item->upja->province_id = isset($item->upja->village->subdistrict->district->province)?$item->upja->village->subdistrict->district->province->id_province:null;

            $item->upja->district_name = isset($item->upja->village->subdistrict->district)?$item->upja->village->subdistrict->district->district_name:null;
            $item->upja->district_id = isset($item->upja->village->subdistrict->district)?$item->upja->village->subdistrict->district->id_district:null;

            $item->upja->subdistrict_name = isset($item->upja->village->subdistrict)?$item->upja->village->subdistrict->subdistrict_name:null;
            $item->upja->subdistrict_id = isset($item->upja->village->subdistrict)?$item->upja->village->subdistrict->id_subdistrict:null;

            $item->upja->village_name = isset($item->upja->village)?$item->upja->village->village_name:null;
            $item->upja->village_id = isset($item->upja->village)?$item->upja->village->id_village:null;

            unset($item->upja->village);

            if($item->image) {
                $item->image_url = url('services/'.$item->image);
            } else {
                $item->image_url = url('services/default.jpeg');
            }
            return $item;
        });

        if($detail && $data) {
            $data = $data[0];
        }
        return $data;
    }

}
