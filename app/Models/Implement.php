<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use App\Models\BaseModel;
use App\Traits\ModelObserver;

class Implement extends BaseModel
{
    use ModelObserver;

    protected $table = 'ot_implement';
    protected $fillable = ['id_ot', 'name', 'price'];
    protected $autoFill = ['created_by'];

    public static function boot()
    {
        parent::boot();

    }

    public static function dataviewsAll()
    {
        $per_page = self::$per_page;

        $filter = Request::get('filter')?:[];

        $data = self::select('*');

        $data = $data->paginate($per_page);

        return $data;
    }

}
