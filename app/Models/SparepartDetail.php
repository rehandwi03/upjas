<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Request;
use App\Models\BaseModel;
use App\Traits\ModelObserver;

class SparepartDetail extends BaseModel
{
    use ModelObserver;

    protected $table = 'ms_sparepart_detail';
    protected $fillable = ['id_sparepart', 'title', 'code', 'partno', 'image', 'qty', 'price', 'is_publish', 'is_hide'];
    protected $autoFill = [];

    public static function boot()
    {
        parent::boot();

    }

    public static function dataviewsAll()
    {
        $per_page = self::$per_page;

        $filter = Request::get('filter')?:[];

        $data = self::select('*');

        $data = $data->paginate($per_page);

        return $data;
    }

}
