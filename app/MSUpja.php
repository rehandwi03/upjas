<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MSUpja extends Model
{
    const CREATED_AT = 'upja_created';
    const UPDATED_AT = 'upja_updated';
    protected $table = 'ms_upja';
    protected $primaryKey = 'id_upja';
    protected $guarded = [];
    protected $hidden = ['upja_password', 'id_province', 'id_district', 'id_subdistrict', 'upja_path'];
    protected $fillable = ['id_village', 'upja_fullname', 'upja_email', 'upja_phone', 'upja_emergency_phone', 'upja_address', 'upja_lat', 'upja_long'];

    public function role()
    {
        $this->belongsTo(MSRole::class, 'id_role');
    }

    public function village()
    {
        return $this->belongsTo('App\MSVillage', 'id_village');
    }

    public function olahTanah()
    {
        return $this->hasMany('App\Models\OlahTanah', 'id_upja');
    }

    public function irigasi()
    {
        return $this->hasMany('App\Models\Irigasi', 'id_upja');
    }

    public function jasaPanen()
    {
        return $this->hasMany('App\Models\JasaPanen', 'id_upja');
    }

    public function jasaTanam()
    {
        return $this->hasMany('App\Models\JasaTanam', 'id_upja');
    }

    public function jualBenih()
    {
        return $this->hasMany('App\Models\JualBenih', 'id_upja');
    }

    public function jualGabah()
    {
        return $this->hasMany('App\Models\JualGabah', 'id_upja');
    }

    public function milling()
    {
        return $this->hasMany('App\Models\Milling', 'id_upja');
    }
}
