<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MSProvince extends Model
{
    const CREATED_AT = 'province_created';
    const UPDATED_AT = 'province_updated';
    protected $table = 'ms_province';
    protected $primaryKey = 'id_province';
    protected $guarded = [];

    public function farmer_address()
    {
        return $this->belongsTo(FarmerAddress::class, 'id_province');
    }

    public function district()
    {
        return $this->hasMany(MSDistrict::class, 'id_province');
    }
}
