<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TransTillage extends Model
{
    public $timestamps = false;
    protected $table = 'trans_tillage';
    protected $primaryKey = 'id_ttillage';
    protected $guarded = [];
}
