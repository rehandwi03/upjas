<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMillingPackingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('milling_packing', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_milling')->unsigned();
			$table->string('type', 30)->nullable();
			$table->decimal('price', 14, 2);
            $table->integer('is_hide')->nullable()->default(0);
            $table->integer('created_by');
            $table->timestamps(0);

            $table->foreign('id_milling')->references('id')->on('ms_milling')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('milling_packing');
	}

}
