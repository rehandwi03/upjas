<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsSparepartTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_sparepart', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('id_upja')->length(5);
            $table->integer('created_by')->length(5);
            $table->string('created_by_role', 10)->nullable();
			$table->string('title', 100)->nullable();
			$table->text('desc', 65535)->nullable();
			$table->string('image', 100)->nullable();
			$table->integer('is_publish')->nullable()->default(0);
            $table->integer('is_hide')->nullable()->default(0);
            $table->timestamps(0);

            $table->foreign('id_upja')->references('id_upja')->on('ms_upja')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_sparepart');
	}

}
