<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrMasterActivity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tr_master_activity', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_tr_master')->unsigned();
            $table->enum('status', array('process','finish','cancel','accept'))->nullable()->default('process');
            $table->integer('is_done')->default(0);
            $table->string('description', 255);
            $table->timestamps();
            $table->integer('created_by')->length(5);
            $table->string('created_by_role', 10)->nullable();

            $table->foreign('id_tr_master')->references('id')->on('tr_master')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tr_master_activity');
    }
}
