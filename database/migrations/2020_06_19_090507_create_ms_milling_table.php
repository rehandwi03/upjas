<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsMillingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_milling', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('id_upja')->length(5);
            $table->integer('created_by')->length(5);
            $table->string('created_by_role', 10)->nullable();
			$table->string('title', 100)->nullable();
			$table->string('image', 100)->nullable();
			$table->decimal('price',14,2)->nullable();
			$table->integer('order_min')->nullable();
			$table->text('terms', 65535)->nullable();
			$table->integer('is_publish')->nullable()->default(0);
            $table->integer('is_hide')->nullable()->default(0);
            $table->timestamps(0);

            $table->foreign('id_upja')->references('id_upja')->on('ms_upja')->onUpdate('CASCADE')->onDelete('CASCADE');

            $table->string('uom_list', 50)->nullable();
            $table->integer('uom_default')->length(10)->nullable();
			$table->foreign('uom_default')->references('id')->on('upja_uom')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_milling');
	}

}
