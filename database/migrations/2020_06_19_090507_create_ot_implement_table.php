<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOtImplementTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ot_implement', function(Blueprint $table)
		{
            $table->increments('id');
			$table->integer('id_ot')->unsigned();
			$table->string('name', 100);
            $table->decimal('price', 14, 2)->nullable();
            $table->integer('created_by');
            $table->timestamps(0);

            $table->foreign('id_ot')->references('id')->on('ms_olah_tanah')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ot_implement');
	}

}
