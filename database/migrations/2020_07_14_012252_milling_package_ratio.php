<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MillingPackageRatio extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('milling_packing', function (Blueprint $table) {
            $table->float('quantity');
            $table->integer('uom')->length(10)->nullable();
			$table->foreign('uom')->references('id')->on('upja_uom')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('milling_packing', function (Blueprint $table) {
            $table->dropColumn('quantity');
            $table->dropColumn('uom');
        });
    }
}
