<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrMasterTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_master', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_upja')->length(5);
			$table->integer('id_farmer')->length(5);
			$table->integer('id_village');
			$table->integer('id_service')->unsigned();
			$table->enum('type', array('ot','irigasi','jt','jp','milling','jb','jg','power','corn','dryer'))->nullable();
			$table->decimal('price_total', 14, 2)->nullable();
			$table->integer('qty_total');
			$table->date('dealing_date')->nullable();
			$table->integer('is_other')->default(0);
			$table->text('other_address', 65535);
			$table->date('date_approved')->nullable();
			$table->integer('is_paid')->nullable();
			$table->enum('status', array('process','finish','cancel','accept'))->nullable()->default('process');
			$table->integer('is_approved')->default(0);
			$table->text('cancel_reason', 65535)->nullable();
			$table->enum('payment_method', array('cod','transfer'))->default('transfer');
            $table->integer('is_hide')->nullable()->default(0);
            $table->timestamps(0);
            $table->integer('created_by')->length(5);
            $table->string('created_by_role', 10)->nullable();

            $table->foreign('id_upja')->references('id_upja')->on('ms_upja')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('id_farmer')->references('id_farmer')->on('ms_farmer')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_master');
	}

}
