<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsNotifTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_notif', function(Blueprint $table)
		{
            $table->increments('id');
            $table->integer('id_upja')->length(5);
			$table->integer('id_trans')->default(0);
			$table->enum('trans_type', array('master','training','sparepart'));
            $table->text('notif_content', 65535)->nullable();
            $table->timestamps(0);

            $table->foreign('id_upja')->references('id_upja')->on('ms_upja')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_notif');
	}

}
