<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMsVarietasTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_varietas', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name', 100);
			$table->text('desc', 65535)->nullable();
            $table->integer('is_hide')->nullable()->default(0);
            $table->timestamps(0);
            $table->integer('created_by');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('ms_varietas');
	}

}
