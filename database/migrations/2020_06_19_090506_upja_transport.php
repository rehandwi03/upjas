<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpjaTransport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('upja_transport', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_upja')->length(5);
            $table->enum('type', array('ot','irigasi','jt','jp','milling','jb','jg','power','corn',
            'dryer'));
            $table->enum('region', array('satu_desa','antar_desa','antar_kecamatan','antar_kota'));
            $table->decimal('price', 14, 2);
            $table->integer('created_by')->length(5);
            $table->string('created_by_role', 10)->nullable();
            $table->timestamps();

            $table->foreign('id_upja')->references('id_upja')->on('ms_upja')->onUpdate('CASCADE')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('upja_transport');
    }
}
