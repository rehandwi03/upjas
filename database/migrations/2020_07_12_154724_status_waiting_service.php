<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class StatusWaitingService extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tr_master', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('tr_master', function (Blueprint $table) {
            $table->enum('status', array('waiting', 'process','finish','cancel','accept'))->nullable()->default('waiting');
        });

        Schema::table('tr_sparepart', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('tr_sparepart', function (Blueprint $table) {
            $table->enum('status', array('waiting', 'process','finish','cancel','accept'))->nullable()->default('waiting');
        });

        Schema::table('tr_training', function (Blueprint $table) {
            $table->dropColumn('status');
        });

        Schema::table('tr_training', function (Blueprint $table) {
            $table->enum('status', array('waiting', 'process','finish','cancel','accept'))->nullable()->default('waiting');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
