<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class TrMasterFarmerAddress extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tr_master', function (Blueprint $table) {
            $table->string('address', 255)->nullable();
            $table->integer('id_faddress')->length(10)->nullable();
            $table->foreign('id_faddress')->references('id_faddress')->on('farmer_address')->onUpdate('CASCADE')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tr_master', function (Blueprint $table) {
            $table->dropColumn('address');
            $table->dropColumn('id_faddress');
        });
    }
}
