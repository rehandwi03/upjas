<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterIdUpjaUom extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('upja_uom', function(Blueprint $table)
		{
			$table->renameColumn('id_uom', 'id');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('upja_uom', function(Blueprint $table)
		{
			$table->renameColumn('id', 'id_uom');
		});
    }
}
