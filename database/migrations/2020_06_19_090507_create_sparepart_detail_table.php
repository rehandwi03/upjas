<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSparepartDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('ms_sparepart_detail', function(Blueprint $table)
		{
            $table->increments('id');
			$table->integer('id_sparepart')->unsigned();
			$table->string('title', 100)->nullable();
			$table->string('code', 100)->nullable();
			$table->string('partno', 100)->nullable();
			$table->string('image', 100)->nullable();
			$table->integer('qty')->nullable();
			$table->decimal('price',14,2)->nullable();
			$table->integer('is_publish')->nullable()->default(0);
            $table->integer('is_hide')->nullable()->default(0);
            $table->timestamps(0);

            $table->foreign('id_sparepart')->references('id')->on('ms_sparepart')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('ms_sparepart_detail');
	}

}
