<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrSparepartTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_sparepart', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_farmer')->length(5);
			$table->integer('id_sparepart')->unsigned();
			$table->integer('qty')->nullable();
			$table->decimal('price_total', 14, 2)->nullable();
			$table->text('note', 65535)->nullable();
			$table->enum('status', array('process','finish','cancel','accept'))->nullable()->default('process');
			$table->integer('is_other')->default(0);
			$table->text('other_address', 65535);
			$table->text('cancel_reason', 65535);
			$table->enum('payment_method', array('cod','transfer'))->default('transfer');
			$table->string('chat_room', 100);
            $table->integer('is_hide')->default(0);
            $table->timestamps(0);
            $table->integer('created_by')->length(5);
            $table->string('created_by_role', 10)->nullable();

            $table->foreign('id_farmer')->references('id_farmer')->on('ms_farmer')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('id_sparepart')->references('id')->on('ms_sparepart')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_sparepart');
	}

}
