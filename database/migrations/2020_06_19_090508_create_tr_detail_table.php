<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_detail', function(Blueprint $table)
		{
            $table->increments('id');
			$table->integer('id_tr_master')->unsigned();
			$table->integer('id_ot_implement')->unsigned()->nullable();
			$table->integer('id_upja_transport')->unsigned()->nullable();
			$table->boolean('is_master')->default(0);
			$table->decimal('price',14,2);
			$table->integer('qty')->nullable();
			$table->string('unit', 50)->nullable();
            $table->decimal('total', 14, 2);

            $table->foreign('id_tr_master')->references('id')->on('tr_master')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('id_ot_implement')->references('id')->on('ot_implement')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('id_upja_transport')->references('id')->on('upja_transport')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_detail');
	}

}
