<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrTrainingTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_training', function(Blueprint $table)
		{
            $table->increments('id');
			$table->integer('id_farmer')->length(5);
			$table->integer('id_training')->unsigned();
			$table->date('start')->nullable();
			$table->date('end')->nullable();
			$table->integer('is_attend')->nullable();
			$table->text('note', 65535)->nullable();
			$table->decimal('price_total', 14, 2)->nullable();
			$table->enum('status', array('process','finish','cancel','accept'))->nullable()->default('process');
			$table->text('cancel_reason', 65535);
			$table->enum('payment_method', array('cod','transfer'))->default('transfer');
			$table->string('chat_room', 100);
            $table->integer('is_hide')->default(0);
            $table->timestamps(0);
            $table->integer('created_by')->length(5);
            $table->string('created_by_role', 10)->nullable();

            $table->foreign('id_farmer')->references('id_farmer')->on('ms_farmer')->onUpdate('CASCADE')->onDelete('RESTRICT');
            $table->foreign('id_training')->references('id')->on('ms_training')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_training');
	}

}
