<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrSparepartDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_sparepart_detail', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_tr_sparepart')->unsigned();
			$table->integer('id_sparepart_detail')->unsigned();
			$table->integer('qty');
			$table->decimal('price', 14, 2);
            $table->decimal('total', 14, 2);

            $table->foreign('id_tr_sparepart')->references('id')->on('tr_sparepart')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('id_sparepart_detail')->references('id')->on('ms_sparepart_detail')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_sparepart_detail');
	}

}
