<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrainingDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('training_detail', function(Blueprint $table)
		{
            $table->increments('id');
			$table->integer('id_training')->unsigned();
			$table->string('title', 100)->nullable();
			$table->decimal('price',14,2)->nullable();
			$table->integer('is_hide')->nullable()->default(0);
            $table->timestamps(0);

            $table->foreign('id_training')->references('id')->on('ms_training')->onUpdate('CASCADE')->onDelete('CASCADE');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('training_detail');
	}

}
