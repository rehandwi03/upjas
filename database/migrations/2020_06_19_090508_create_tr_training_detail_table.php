<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTrTrainingDetailTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('tr_training_detail', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('id_tr_training')->unsigned();
            $table->integer('id_training_detail')->unsigned();
            $table->decimal('price', 14, 2);

            $table->foreign('id_tr_training')->references('id')->on('ms_training')->onUpdate('CASCADE')->onDelete('CASCADE');
            $table->foreign('id_training_detail')->references('id')->on('tr_training_detail')->onUpdate('CASCADE')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('tr_training_detail');
	}

}
