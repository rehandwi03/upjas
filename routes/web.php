<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});
// endpoint auth
$router->post('/auth/login', 'AuthController@login');
$router->post('/register/farmer', 'FarmerController@store');
$router->get('/health', function () {
    $respon = ["message" => "health"];
    return response()->json($respon, 200);
});
$router->group(
    ['middleware' => 'jwt.auth'],
    function () use ($router) {
        // endpoint home
        $router->get('/home', function () {
            return response()->json([
                "message" => "success",
                "expired" => false
            ], 200);
        });
        // endpoint trans tillage
        $router->get('/transtillages', 'TransTillageController@index');
        $router->post('/transtillage', 'TransTillageController@store');
        // endpoint trans journey
        $router->get('/transjourneys', 'TransJourneyController@index');
        $router->post('/transjourney', 'TransJourneyController@store');
        $router->patch('/transjourney/update/{id}', 'TransJourneyController@update');

        // endpoint get province, dll
        $router->get('/provinces', 'ProvinceController@index');
        $router->get('/districts/{id_province}', 'DistrictController@index');
        $router->get('/subdistricts/{id_district}', 'SubdistrictController@index');
        $router->get('/villages/{id_subdistrict}', 'VillageController@index');

        $router->post('file/upload', 'FileController@upload');

        $router->post('data/save/{model}', 'DataController@save');
        $router->post('data/delete/{model}', 'DataController@delete');
        $router->post('data/changeValue/{model}', 'DataController@changeValue');
        $router->get('data/get/{model}', 'DataController@get');
        $router->get('data/detail/{model}/{id}', 'DataController@detail');
        $router->get('autocomplete/{model}', 'DataController@autocomplete');
        $router->get('get_options/{model}', 'DataController@getOptions');
        $router->get('upja/list', 'UpjaController@list');

        $router->group(['middleware' => 'admin'], function () use ($router) {
            // endpoint role
            $router->get('/roles', 'RoleController@index');
            $router->post('/role', 'RoleController@store');
            $router->patch('/role/update/{id}', 'RoleController@update');
            $router->patch('/role/hide/{id}', 'RoleController@role_hide');

            // endpoint admin
            $router->get('/admins', 'AdminController@index');
            $router->post('/admin', 'AdminController@store');
            $router->patch('/admin/hide/{id}', 'AdminController@hide');
            $router->patch('/admin/update/{id}', 'AdminController@update');

            // endpoint bank
            $router->get('/banks', 'BankController@index');
            $router->post('/bank', 'BankController@store');
            $router->patch('/bank/publish/{id}', 'BankController@bank_publish');
            $router->patch('/bank/hide/{id}', 'BankController@bank_hide');

            // endpoint province

            $router->post('/province', 'ProvinceController@store');
            $router->patch('/province/hide/{id}', 'ProvinceController@province_hide');
            $router->patch('/province/update/{id}', 'ProvinceController@update');

            //endpoint district
            $router->get('/districts', 'DistrictController@index');
            $router->post('/district', 'DistrictController@store');
            $router->patch('/district/hide/{id}', 'DistrictController@district_hide');
            $router->patch('/district/update/{id}', 'DistrictController@update');

            // endpoint subdistrict
            $router->get('/subdistricts', 'SubdistrictController@index');
            $router->post('/subdistrict', 'SubdistrictController@store');
            $router->patch('/subdistrict/hide/{id}', 'SubdistrictController@subdistrict_hide');
            $router->patch('/subdistrict/update/{id}', 'SubdistrictController@update');

            // endpoint village
            $router->get('/villages', 'VillageController@index');
            $router->post('/village', 'VillageController@store');
            $router->patch('/village/hide/{id}', 'VillageController@village_hide');
            $router->patch('/village/update/{id}', 'VillageController@update');

            // endpoint transport
            $router->get('/transports', 'TransportController@index');
            $router->post('/transport', 'TransportController@store');
            $router->patch('/transport/hide/{id}', 'TransportController@transport_hide');
            $router->patch('/transport/update/{id}', 'TransportController@update');

            // endpoint trans
            $router->get('/transactions', 'TransController@index');
            $router->post('/transaction', 'TransController@store');
            $router->patch('/transaction/status/{id}', 'TransController@trans_status');
        });

        // endpoint farmer
        $router->group(['middleware' => ['farmer']], function () use ($router) {
            $router->get('/farmers', 'FarmerController@index');
            $router->patch('/farmer/status/{id}', 'FarmerController@farmer_status');
            $router->patch('/farmer/hide/{id}', 'FarmerController@farmer_hide');
            $router->patch('/farmer/verify/{id}', 'FarmerController@verify_code');
            $router->get('/farmer/profile', 'FarmerController@show');
            $router->post('/farmer/profile', 'FarmerController@update');
            $router->post('/farmer/password', 'FarmerController@change_password');
            // endpoint farmer address
            $router->get('/faddress', 'FAddressController@index');
            //$router->get('/faddress/show', 'FAddressController@id_farmer');
            $router->post('/faddress', 'FAddressController@store');
            $router->post('/faddress/delete', 'FAddressController@delete');
        });

        // endpoint upja
        $router->group(['middleware' => 'upja'], function () use ($router) {
            // endpoint master upja
            $router->get('/upjas', 'UpjaController@index');
            $router->get('/upja/profile', 'UpjaController@show');
            $router->post('/upja/profile', 'UpjaController@update');
            $router->get('/upja/performance', 'UpjaController@performance');
            $router->post('/upja', 'UpjaController@store');
            $router->patch('/upja/status/{id}', 'UpjaController@upja_status');
            $router->patch('/upja/verified/{id}', 'UpjaController@upja_verified');
            $router->patch('/upja/hide/{id}', 'UpjaController@upja_hide');
            $router->post('/upja/password', 'UpjaController@change_password');

            // endpoint upja uom
            $router->get('/upja/uoms', 'UpjaUomController@index');
            $router->post('/upja/uom', 'UpjaUomController@store');
            $router->patch('/upja/uom/hide/{id}', 'UpjaUomController@uom_hide');
            $router->patch('/upja/uom/update/{id}', 'UpjaUomController@update');
        });
    }
);
